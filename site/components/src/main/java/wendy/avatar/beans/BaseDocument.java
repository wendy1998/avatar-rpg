package wendy.avatar.beans;

import org.hippoecm.hst.content.beans.Node;
import org.hippoecm.hst.content.beans.standard.HippoDocument;

@Node(jcrType="avatar:basedocument")
public class BaseDocument extends HippoDocument {

}
