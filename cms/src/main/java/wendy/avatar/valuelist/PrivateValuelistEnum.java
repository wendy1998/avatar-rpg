package wendy.avatar.valuelist;

public interface PrivateValuelistEnum {

    default String getDisplay(){return getDisplay(null);};

    String getDisplay(String language);

    String getKey();
}
